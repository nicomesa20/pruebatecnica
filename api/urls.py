""" Contains api urls paths """

from django.urls import include, path
from .views.brand import SpecificBrandApi
from .views.user import SpecificUserApi


urlpatterns = [
    path('register/', SpecificUserApi.as_view()),
    # path('brands', ApiEndpoint.as_view()),  # an example resource endpoint
    path('brands/<int:pk>', SpecificBrandApi.as_view()),  # an example resource endpoint
]