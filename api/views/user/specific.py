""" Contains User endpoint definition """

from django.contrib.auth.hashers import make_password
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import permission_classes
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from cerberus import Validator

from prueba_tecnica.settings import CLIENT_SECRET, CLIENT_ID
from ...models.user import User

import requests

class SpecificUserApi(APIView):
    """ Defines the HTTP verbs to user model management. """

    @permission_classes([AllowAny])
    def post(self, request):  # pylint: disable=no-self-use
        """ Creates a new user.

        Parameters
        ----------
            request: dict
                Contains http transaction information.

        Returns
        -------
            int: a status code.

        """
        validator = Validator({
            "username": {"required": True, "type": "string"},
            "password": {"required": True, "type": "string"},
        })
        if not validator.validate(request.data):
            return Response({
                "code": "invalid_body",
                "detailed": "Cuerpo de la petición con estructura inválida",
                "data": validator.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        if not User.objects.filter(username=request.data['username']):
            User.objects.create(password=make_password(request.data['password']), username=request.data['username'])

        r = requests.post('http://127.0.0.1:8000/o/token/',
        data={
            'grant_type': 'password',
            'username': request.data['username'],
            'password': request.data['password'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET
        })
        return Response(r.json())