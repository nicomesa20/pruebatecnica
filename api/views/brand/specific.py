""" Handles specific model methods """

from cerberus import Validator
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from oauth2_provider.views.generic import ProtectedResourceView
from django.contrib.auth.decorators import login_required

# pylint: disable=too-many-ancestors
class SpecificBrandApi(ProtectedResourceView):
    """ Defines the HTTP verb to brand model management """

    def get(self, request, pk, *args, **kwargs): 
        """ Return brand information.

        Parameters:
            request (dict):Contains http transaction information.
            pk (int): pk of the brand to me shown
        Returns:
            Response (dict,status):Brand information and a status code.

        """

        return HttpResponse('Hello, OAuth2!')
        
         