""" Contains store model """

from django.db import models 
from .store import Store

class Deal(models.Model):
    """ Store model definition """

    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    name = models.CharField(max_length=45)
    identifier = models.CharField(max_length=45)
    image = models.CharField(max_length=200, blank=True, null=True)
    price = models.PositiveIntegerField()

    class Meta:  # pylint: disable=too-few-public-methods
        """ Sets human readable name """
        verbose_name = "Oferta"
        verbose_name_plural = "Oferta"

    def __str__(self):
        return self.name+self.price