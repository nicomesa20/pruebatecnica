""" Contains store model """

from django.db import models 
from .brand import Brand

class Store(models.Model):
    """ Store model definition """

    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    name = models.CharField(max_length=45)
    identifier = models.CharField(max_length=45)
    thumbnail = models.CharField(max_length=200, blank=True, null=True)
    address = models.CharField(max_length=45)

    class Meta:  # pylint: disable=too-few-public-methods
        """ Sets human readable name """
        verbose_name = "Tienda"
        verbose_name_plural = "Tiendas"

    def __str__(self):
        return self.name+self.brand