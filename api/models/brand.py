""" Contains brand model """

from django.db import models

class Brand(models.Model):
    """ Brand model definition """
    
    name = models.CharField(max_length=45)
    logo = models.CharField(max_length=200)

    class Meta:  # pylint: disable=too-few-public-methods
        """ Sets human readable name """
        verbose_name = "Marca"
        verbose_name_plural = "Marcas"

    def __str__(self):
        return self.name